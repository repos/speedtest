#!/bin/sh
# ./speedtest.sh http://somewhere.with/a/100MB.file


speedtest ()
{
	FQDN=$(echo "${TARGET}" | sed -e 's|^[^/]*//||' -e 's|/.*$||')
	EPOCH=$(date +"%s")

	wget $1 -q --show-progress -O - > /dev/null

	SPEED=$(expr 8 \* 100 / $(($(date +"%s")-${EPOCH})))

	echo "${SPEED} Mb/s"
	echo "${EPOCH},${SPEED}" >> ${FQDN}.csv
}

while true
do
	for TARGET in ${@}
	do
		echo "Testing ${TARGET}"
		speedtest ${TARGET}
	done
	#sleep 10m
done


